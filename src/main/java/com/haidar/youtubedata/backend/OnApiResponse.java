package com.haidar.youtubedata.backend;

import android.support.annotation.NonNull;

public interface OnApiResponse<T> {
    void onSuccess(@NonNull T response);
    void onError();
    void onResponse();
}
