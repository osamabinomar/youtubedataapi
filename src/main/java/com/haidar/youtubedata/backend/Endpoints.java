package com.haidar.youtubedata.backend;

import com.haidar.youtubedata.domain.base.BaseResponse;
import com.haidar.youtubedata.domain.playlist.Playlist;
import com.haidar.youtubedata.domain.video.YoutubeVideo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Endpoints {
    @GET("playlistItems?part=snippet")
    Call<BaseResponse<YoutubeVideo>> getPlaylistVideos(
            @Query("playlistId") String playlistId,
            @Query("pageToken") String pageToken,
            @Query("key") String apiKey);

    @GET("playlists?part=snippet&maxResults=20")
    Call<BaseResponse<Playlist>> getChannelPlaylists(@Query("channelId") String channelId,
                                                     @Query("pageToken") String page,
                                                     @Query("key") String key);

}
