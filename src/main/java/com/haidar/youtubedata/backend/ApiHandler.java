package com.haidar.youtubedata.backend;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.haidar.youtubedata.domain.base.BaseResponse;
import com.haidar.youtubedata.domain.playlist.Playlist;
import com.haidar.youtubedata.domain.video.YoutubeVideo;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiHandler {
    private static final String BASE_URL="https://www.googleapis.com/youtube/v3/";
    private static  String API_KEY;
    private final Endpoints endpoints;
    private static ApiHandler instance;
    private ApiHandler(){
        endpoints=new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(Endpoints.class);
    }

    public static ApiHandler getInstance() {
        if (API_KEY==null){
            throw new IllegalArgumentException("Please set youtube api key first.");
        }
        if (instance==null){
            instance=new ApiHandler();
        }
        return instance;
    }

    public static void setKey(@NonNull String key){
        API_KEY=key;
    }

    public void loadPlaylistVideos(@NonNull String playlistId, @Nullable String page,
                                   @NonNull OnApiResponse<BaseResponse<YoutubeVideo>> completion){
        Call<BaseResponse<YoutubeVideo>> call=endpoints.getPlaylistVideos(playlistId,page,API_KEY);
        call.enqueue(new Callback<>(completion));
    }

    public void loadChannelPlaylists(@NonNull String channelId, @Nullable String page,
                                     @NonNull OnApiResponse<BaseResponse<Playlist>> completion){
        Call<BaseResponse<Playlist>> call= endpoints.getChannelPlaylists(channelId,page,API_KEY);
        call.enqueue(new Callback<>(completion));
    }
}
