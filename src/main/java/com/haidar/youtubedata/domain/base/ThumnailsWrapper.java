package com.haidar.youtubedata.domain.base;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ThumnailsWrapper implements Serializable {
    @SerializedName("default")
    private Thumbnail defaultThumb;
    private Thumbnail medium;
    private Thumbnail high;
    private Thumbnail standard;
    private Thumbnail maxres;

    public Thumbnail getDefaultThumb() {
        return defaultThumb;
    }

    public void setDefaultThumb(Thumbnail defaultThumb) {
        this.defaultThumb = defaultThumb;
    }

    public Thumbnail getMedium() {
        return medium;
    }

    public void setMedium(Thumbnail medium) {
        this.medium = medium;
    }

    public Thumbnail getHigh() {
        return high;
    }

    public void setHigh(Thumbnail high) {
        this.high = high;
    }

    public Thumbnail getStandard() {
        return standard;
    }

    public void setStandard(Thumbnail standard) {
        this.standard = standard;
    }

    public Thumbnail getMaxres() {
        return maxres;
    }

    public void setMaxres(Thumbnail maxres) {
        this.maxres = maxres;
    }
}
