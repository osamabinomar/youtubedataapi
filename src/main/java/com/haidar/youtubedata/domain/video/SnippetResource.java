package com.haidar.youtubedata.domain.video;

import java.io.Serializable;

public class SnippetResource implements Serializable {
    private String kind;
    private String videoId;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getVideo() {
        return videoId;
    }

    public void setVideo(String video) {
        this.videoId = video;
    }
}
