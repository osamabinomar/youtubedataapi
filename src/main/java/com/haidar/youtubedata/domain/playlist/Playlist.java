package com.haidar.youtubedata.domain.playlist;

import com.haidar.youtubedata.domain.base.BaseSnippet;

import java.io.Serializable;

public class Playlist implements Serializable {
    private String id;
    private BaseSnippet snippet;
    private String etag;
    private String kind;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BaseSnippet getSnippet() {
        return snippet;
    }

    public void setSnippet(BaseSnippet snippet) {
        this.snippet = snippet;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }
}
